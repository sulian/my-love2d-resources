# My Löve resources
![Löve2d](logo.png){width=50%}

My personnal collection of [LÖVE](http://love2d.org) libraries, tutorials, projects and resources.

## Contents

[[_TOC_]]

## Lua
*Resources for Lua*

- [wxlua](http://wxlua.free.fr/Tutoriel_Lua/Tuto/Fonctions/methodes.php) - French odcumentation on  fonction, method, self or this
- [stackoverflow - 22564978](https://stackoverflow.com/questions/22564978/difference-between-and-in-lua) - Difference between "." and ":" in lua
- [Corona Labs](https://docs.coronalabs.com/tutorial/data/luaStringMagic/index.html) - Lua String Magic

## Löve2d

- [Löve - community blogs](https://blogs.love2d.org/)
- [KeyConstant](https://love2d.org/wiki/KeyConstant)
- [Code optimization](https://love2d.org/forums/viewtopic.php?t=89923)
- [Question about local and global / _G](https://love2d.org/forums/viewtopic.php?t=86599)

## Librairies

- [LoveLib](https://github.com/besnoi/lovelib)
- [TLfres](https://love2d.org/wiki/TLfres)

## Debug

- [LoveDebug](https://github.com/flamendless/lovedebug)
- [Love2D simple logger](https://yal.cc/love2d-simple-logger/)
- [Garbage Collection in LÖVE](https://blogs.love2d.org/content/garbage-collection-löve)
- https://love2d.org/forums/viewtopic.php?t=81718
- https://love2d.org/forums/viewtopic.php?t=93800
- https://love2d.org/forums/viewtopic.php?t=79579

## GitLab

- [GitLab projects](https://gitlab.com/explore/projects/topics/love2d)

## Camera

- [Cameras and canvases](https://sheepolution.com/learn/book/22)
- [Gamera](https://github.com/kikito/gamera)
- [Gamera / minimap](https://love2d.org/forums/viewtopic.php?f=5&t=12033)

## Minimap

- [Minimap 1](https://love2d.org/forums/viewtopic.php?f=5&t=12033)
- [Minimap 2](https://www.reddit.com/r/Unity3D/comments/s4ynd1/trying_to_create_a_minimap_for_my_game_looking/)
- [Minimap 3](https://love2d.org/forums/viewtopic.php?f=4&t=24027)
- [Minimap 4](https://www.reddit.com/r/Unity3D/comments/s4ynd1/trying_to_create_a_minimap_for_my_game_looking/)
- [Minimap 5](https://love2d.org/forums/viewtopic.php?t=76861)
- [Minimap 6](https://love2d.org/forums/viewtopic.php?t=9385)
- [Minimap 7](https://love2d.org/forums/viewtopic.php?t=81312)

## Tilesets / tilemaps

- [Marching squares](https://en.wikipedia.org/wiki/Marching_squares)
- [Ivan Skodje - Marching Squares](https://ivanskodje.com/marching-squares/)

- https://developer.mozilla.org/en-US/docs/Games/Techniques/Tilemaps
- https://www.sandromaglione.com/articles/how-to-create-a-pixel-art-tileset-complete-guide
- https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/tilemap-based-game-techniques-base-data-struc-r837/
- https://www.gamedev.net/articles/programming/general-and-gameplay-programming/tilemap-based-game-techniques-handling-terrai-r934/
- https://forum.unity.com/threads/tile-transition-rule-tile-improvement-corner-based-rules.519623/
- https://www.squidi.net/three/entry.php?id=166

### BorisTheBrave

- [Tileset Roundup](https://www.boristhebrave.com/2013/07/14/tileset-roundup/)
- [Classification of Tilesets](https://www.boristhebrave.com/2021/11/14/classification-of-tilesets/)
- [Quarter-Tile Autotiling](https://www.boristhebrave.com/2023/05/31/quarter-tile-autotiling/)
- [Beyond Basic Autotiling](https://www.boristhebrave.com/2021/09/12/beyond-basic-autotiling/)
- [2d Marching Cubes with Multiple Colors](https://www.boristhebrave.com/2021/12/29/2d-marching-cubes-with-multiple-colors/)
- [Ortho-tiles](https://www.boristhebrave.com/2023/05/31/ortho-tiles/)
- [Wang Tileset Creator](https://www.boristhebrave.com/2023/06/04/wang-tileset-creator/)
    - [Tool](https://www.boristhebrave.com/permanent/23/03/tileset-creator/)

### Sandro Maglione

- [Articles](https://www.sandromaglione.com/articles)
- [How to create a Pixel Art Tileset - Complete Guide](https://www.sandromaglione.com/articles/how-to-create-a-pixel-art-tileset-complete-guide)
- [Pixel Art Game Tileset made easier](https://www.sandromaglione.com/articles/pixel-art-game-tileset-made-easier)

### Tilesetter

- [Tilesetter](https://www.tilesetter.org/)
- [Documentation](https://www.tilesetter.org/docs/)

## 9Patch

- https://love2d.org/forums/viewtopic.php?t=94687
- https://love2d.org/forums/viewtopic.php?t=82380

## Effects

- [Glitch shader?](https://love2d.org/forums/viewtopic.php?t=87653)
- [Postprocessing effect repository for LÖVE](https://github.com/idbrii/love-moonshine)
- [Glow effect (*without Moonshine*)](https://ebens.me/posts/glow-effect-for-lined-shapes-in-love2d)

### Moonshine

- [Moonshine](git clone https://github.com/vrld/moonshine.git)

## Circle

- [Circular bar](https://love2d.org/forums/viewtopic.php?t=76466)
- [Circle collision 1](https://love2d.org/forums/viewtopic.php?t=2736)
- [Circle collision 2](https://blogs.love2d.org/content/circle-collisions)
- [Circle collision 3](https://love2d.org/forums/viewtopic.php?t=89671)

## Misc

- [Sheepolution](https://sheepolution.com/learn/book/contents)
- [vscode + LUA / Löve2D](https://stackoverflow.com/questions/65066037/how-to-debug-lua-love2d-with-vscode/65066145#65066145)
- [Lynn's Legacy](https://github.com/gradualgames/lynnslegacy)
- [glitchsokoban](https://codeberg.org/glitchapp/glitch-sokoban)
- [Simple game tutorials](https://github.com/simplegametutorials/simplegametutorials)
- [Simple Game Tutorials for Lua and LÖVE 11](https://simplegametutorials.github.io/love/)
- [Arkanoid](https://github.com/besnoi/arkanoid)

- https://docs.coronalabs.com/tutorial/basics/colonDotOperators/index.html
- https://docs.coronalabs.com/tutorial/basics/globals/index.html
- https://docs.coronalabs.com/tutorial/basics/scope/index.html
- https://docs.coronalabs.com/tutorial/basics/codeFormatting/index.html
- https://docs.coronalabs.com/tutorial/basics/functionArguments/index.html
- https://docs.coronalabs.com/tutorial/basics/anonymousFunctions/index.html
- https://docs.coronalabs.com/tutorial/basics/externalModules/index.html
- https://docs.coronalabs.com/tutorial/games/keepScores/index.html
- https://docs.coronalabs.com/tutorial/data/outputTable/index.html
- https://love2d.org/forums/viewtopic.php?t=3056
- https://love2d.org/forums/viewtopic.php?t=81270
- https://love2d.org/forums/viewtopic.php?t=77327
- https://gameuidatabase.com/gameData.php?id=1433
- https://www.gamecodeur.fr/dlc-93-tracer-son-code-debug-malin/
- https://love2d.org/forums/viewtopic.php?t=81312
- https://github.com/kyleschaub/cavern
